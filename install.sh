apt-get update
apt-get install -y python3-pip postgresql postgresql-contrib libpq-dev nginx udhcpd hostapd

echo "host all all 127.0.0.1/32 trust" > /tmp/pg_hba.conf
sudo mv /tmp/pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
sudo service postgresql restart

sudo echo -e "postgres\npostgres" | sudo passwd postgres
sudo su postgres <<EOF
  createuser library -h127.0.0.1 -Upostgres
  createdb library -h127.0.0.1 -Upostgres
  psql -h127.0.0.1 -Upostgres -c "grant all privileges on database library to library";  
EOF

sudo easy_install3 -U pip
sudo pip3 install -r requirements.txt
sudo pip3 install uwsgi

echo "export DATABASE_URL=postgresql://library@127.0.0.1:5432/library" > ~/.profile
export DATABASE_URL=postgresql://library@127.0.0.1:5432/library

python3 manage.py migrate

sudo mkdir -p /home/pirate/open-library/modules
sudo python3 manage.py collectstatic --noinput
python3 manage.py createsuperuserwithpassword --username=admin --password=pleasechangeme --email=test@test.com --noinput

sudo ln -s /home/pirate/open-library/nginx.conf /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default

sudo mkdir -p /etc/uwsgi
sudo mkdir -p /etc/uwsgi/vassals
sudo ln -s /home/pirate/open-library/uwsgi.ini /etc/uwsgi/vassals/

sudo touch /var/log/uwsgi-emperor.log
sudo chown www-data:www-data /var/log/uwsgi-emperor.log
sudo chmod a+rwx /var/log/uwsgi-emperor.log

sudo rm /etc/rc.local
sudo ln -s /home/pirate/open-library/rc.local /etc/rc.local
sudo chmod 775 /etc/rc.local
sudo chown root /etc/rc.local

sudo rm -f /etc/udhcpd.conf
sudo ln -s /home/pirate/open-library/udhcpd.conf /etc/udhcpd.conf
sudo echo "DHCPD_OPTS=\"-S\"" > /tmp/udhcpd
sudo cp /tmp/udhcpd /etc/default/udhcpd

sudo ln -s /home/pirate/open-library/wlan0 /etc/network/interfaces.d/wlan0
sudo ln -s /home/pirate/open-library/hostapd.conf /etc/hostapd/hostapd.conf
sudo echo "DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"" > /tmp/hostapd
sudo cp /tmp/hostapd /etc/default/hostapd
