# Quick Start

*If you only need to run the application, follow these steps!*

1. [Install Docker Compose](https://docs.docker.com/compose/install/)
2. Clone this repository using `git clone [url]`
3. From the root of the cloned repository, run `docker-compose up`

You should now be able to access the application on port 80 (default web port) of the machine where Docker is running. On Windows and macOS, Docker will be running on a virtual machine.  You can find the IP address of this machine by running `docker-machine ip`.  For me, this yields `192.168.99.100`, so I can visit `http://192.168.99.100` in my web browser to find the application.

TODO: Add instructions for Linux (does it launch on localhost:80?)

To update the application:

1. Run `docker-compose down` to kill any existing containers
2. Run `git pull` to download the latest changes
3. Run `docker-compose up` to restart the application

Note that this will destroy the database and any existing data.


# Requirements

- [Python 3.6.1](https://www.python.org/downloads/release/python-361/) 
    
    **recommended installation:**
        
    1) Install [pyenv](https://github.com/pyenv/pyenv)
    
    2) run `pyenv install 3.6.1`).  
    
    **If you are using pyenv, all `pip` or `python` commands MUST be prepended by `pyenv exec [..command]`.**
- [Postgres](https://www.postgresql.org/download/)

# Development Setup

1. Clone this repository locally (`git clone [url]`)
2. _Recommended step_: Install [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv).  Create a virtualenv environment for the project by running `pyenv virtualenv 3.6.1 open_library`.  Activate your virtualenv by running `pyenv activate open_library`.  This will isolate your project from all other Python projects on your machine, eliminating dependency conflicts. 
3. _Recommended step_: Create a `.env` file in the project root.  This file is in the `.gitignore` and will not be checked in to the repository.  Install [autoenv](https://github.com/kennethreitz/autoenv) so that when you move into the current directory, your shell automatically loads the `.env` file.  Inside the `.env` file you can perform tasks like defining environment variables for the project (such as the `DATABASE_URL` that will be discussed below) and activating your virtual environment (`python activate open_library`, as discussed in step 2.).
4. Create a Postgres database for use by the project (for the purposes of this example, we'll use `open_library`). Your database connection string is then `postgres://localhost:5432/open_library`.  If you require authentication, then the connection string is `postgres://username:password@localhost:5432/open_library`.  The `DATABASE_URL` environment must be set to the connection string (i.e. in a BASH shell `export DATABASE_URL=postgres://localhost:5432/open_library`).  This must be done every time you are going to run commands for the project, so it's recommended that you set up autoenv as discussed in step 3.
5. Run `pip install -r requirements.txt` to install all Python dependencies.
6. Run `python manage.py migrate`.  If this fails then you likely have a database connection error (or a Python dependency error).
7. Run `python manage.py createsuperuser` to create a superuser account for logging into the admin panel.


# Running Test Cases

Run `python manage.py test`

# Running the Server

1. Run `python manage.py runserver`
2. Visit `http://localhost:8000` to view the project!

# Common Tasks

**All URLs here are relative to the root; that is, if your server is running at `http://localhost:8000`, then `/admin` refers to `http://localhost:8000/admin`.**

## Creating an Admin Account

1. Visit `/admin`
2. Log in with your superuser account
3. Select `AUTHENTICATION AND AUTHORIZATION > Users > Add`
4. Enter their `Password`
5. Check `Superuser status`
6. Enter their `Username`
7. Click `Save`

## Creating a Student Account

1. Visit `/admin`
2. Log in with your superuser account
3. Select `CORE > Students > Add`
4. Beside `User`, select the `+` to create a new user for the Student.  Fill out their `Username` and `Password`, then click `Save`
5. Fill out any other `Profile` information and click `Save`

## Uploading a Module

1. Visit `/admin`
2. Log in with your superuser account
3. Select `CORE > Modules > Add`
4. Locate your Module Archive
5. Click `Save`

## Viewing Content

1. Visit `/` or `/login`
2. Log in with your account
3. Select a module

## Downloading Data Collected by the Application

1. Visit `/admin`
2. Log in with your superuser account
3. Select `TASKS > Download Data Dump`

# Module Archives

Module archives are `.tar.gz` archives which have the following structure:

```
- module.json (definition of the module's properties)
- injection.html (an HTML snippet to be injected into the main module page)
- content/ (folder containing the module's static content)
```

## Example: Creating a module from EPaath 7-8

1. Create a new folder for you module, which we'll call `epaath-7-8`
2. Create a JSON file `epaath-7-8/module.json` with the following content:

    ```
    {
      "name": "EPaath 7-8",
      "slug": "epaath-7-8",
      "entry_point": "/index.html"
    }
    ```
    
    The `name` property is the friendly name of the module.  The `slug` is used to load the Module; in our example, this module will be available at `/modules/epaath-7-8/`.  The `slug` also defines the folder which the module is installed into.  The `entry_point` defines the first page of the module to load.

3. Create a folder `epaath-7-8/content/`.  This will contain all of the modules actual content.
4. [Download the EPaath archive](http://www.olenepal.org/download-e-paath/) and extract it to a separate folder (which we'll call `epaath-src`)
5. Copy the contents of `epath-src/EPaath7-8` into `epaath-7-8/content/`.  If you did this correctly, you should now have `epaath-7-8/content/index.html`, which is our `entry_point`.
6. Now, we need to create an `injection.html` script which will provide some custom functionality for the module, mainly logging the user's activity as they move through the module:

    ```
    <script type="text/javascript">
    var lastUrl;
    
    setInterval(function () {
      var location = frame.contentWindow.location;
      var currUrl = location.pathname.replace('/static/epaath-7-8', '') + location.search;
    
      if (currUrl !== lastUrl) {
        refreshAccess(currUrl);
        lastUrl = currUrl;
      }
    }, 1000);
    </script>
    ```
    
    There are two special globals used in this script:
    
        - `frame` references the IFrame in which the module content is loaded.
        - `refreshAccess` is a function that should be called to update the user's current state in the application.  In a web application, their state can be uniquely identified by the URL that they are currently visiting.  For other types of applications, a different text-based identifying may need to be used.

7. Finally, we need to create a `.tar.gz` archive from `epaath-7-8`.  To do this in a unix environment:

    ```
    cd epaath-7-8
    tar -cvf epaath-7-8.tar.gz *
    ```
    
    Note that if you're going to run this command again, you need to delete `epaath-7-8.tar.gz` from the folder, otherwise you'll nest an archive in an archive... o_O INCEPTION.


# PI Setup

## Wifi

http://elinux.org/RPI-Wireless-Hotspot

## Project

Setup details are included in `install.sh`
