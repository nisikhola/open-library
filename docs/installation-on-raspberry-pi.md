1. Follow the instructions [here](https://blog.hypriot.com/getting-started-with-docker-on-your-arm-device/) to flash the device & get a shell on it.
2. Run `cd ~`
2. Run `sudo git clone https://gitlab.com/nisikhola/open-library.git`
2. Run `cd open-library`
3. Run `sudo bash open-library/install-os.sh`
3. Run `sudo bash open-library/install.sh`
3. Run `sudo bash /etc/rc.local`