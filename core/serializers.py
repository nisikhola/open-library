from rest_framework import serializers
from .models import *


class AccessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = [
            'id',
            'session',
            'path',
            'start',
            'last_checkin',
            'module',
        ]