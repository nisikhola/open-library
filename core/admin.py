from django.contrib.auth.models import User
from django.contrib.admin.views.main import ChangeList
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from core import forms
from .models import *


class ModuleAdminChangeList(ChangeList):
    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        return queryset.filter(active=True)


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    pass

admin.site.unregister(User)


class StudentInline(admin.StackedInline):
    model = Student
    max_num = 1

@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ('username',)
    list_filter = ('is_staff', 'is_superuser')
    inlines = [
        StudentInline
    ]

    add_form = forms.UserCreateForm
    change_form = forms.UserChangingForm

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = [
        ]
        return super().get_form(request, obj, **kwargs)


# @admin.register(User)
# class UserAdmin(admin.ModelAdmin):
#     form = forms.UserForm
#     inlines = [
#     ]