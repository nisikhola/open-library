from django.utils.translation import ugettext as _
from django import forms
from django.contrib.auth.models import User
from core import models
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class AddModuleForm(forms.ModelForm):
    module = forms.FileField()

    class Meta:
        model = models.Module
        fields = ['module',]


class UserCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username', 
            'password',
            'is_staff',
            'is_superuser',
        ]


class UserChangingForm(UserChangeForm):
    class Meta:
        model = User
        fields = [
            'username', 
            'password',
            'is_staff',
            'is_superuser',
        ]