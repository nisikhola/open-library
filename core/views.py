from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.views.generic import TemplateView, FormView
from django.views.generic.base import View
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from core import encoders
from .models import *
from .serializers import *
from .forms import *
import json
import os


class ModuleView(LoginRequiredMixin, TemplateView):
    template_name = 'module.html'

    def get_context_data(self, *args, **kwargs):
        module_slug = kwargs.get('module')
        module = get_object_or_404(Module, slug=module_slug)
        default_injection = """
        <script type="text/javascript">
        var lastUrl;

        setInterval(function () {{
          var location = frame.contentWindow.location;
          var currUrl = location.pathname.replace('/static/{}', '') + location.search;

          if (currUrl !== lastUrl) {{
            refreshAccess(currUrl);
            lastUrl = currUrl;
          }}
        }}, 1000);
        </script>
        """.format(module.slug)
        context = {
            'module': module,
            'injection': module.injection if module.injection is not None and module.injection != '' else default_injection
        }
        return context


class ModuleListView(LoginRequiredMixin, TemplateView):
    template_name = 'modules.html'

    def get_context_data(self, *args, **kwargs):
        modules = Module.objects.all()
        context = {
            'modules': modules
        }
        return context


class AccessesViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.UpdateModelMixin):
    queryset = Access.objects.all()
    serializer_class = AccessSerializer

    def create(self, request):
        session_id = request.session.get('session_id')

        try:
            session = Session.objects.get(id=session_id)
            session.save() # update last_checkin
        except:
            session = Session.objects.create(user=request.user)
            request.session['session_id'] = session.id

        request.POST._mutable = True
        request.data['session'] = session.id
        return super().create(request)

    def update(self, request, **kwargs):
        access = get_object_or_404(Access, id=int(kwargs.get('pk')))
        access.save()
        access.session.save()
        serializer = AccessSerializer(access)
        return Response(serializer.data)



class LoginView(FormView):
    template_name = 'login.html'
    success_url = '/modules/'
    form_class = AuthenticationForm

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('/modules/')
        else:
            return super().dispatch(request, *args, **kwargs)


    def form_valid(self, form):
        login(self.request, form.get_user())
        user = form.get_user()
        session = Session.objects.create(user=user)
        self.request.session['session_id'] = session.id
        return HttpResponseRedirect(self.get_success_url())


class HomeView(View):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return redirect('/login/')
        else:
            return redirect('/modules/')


class LogoutView(View):
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return redirect('/login/')


class ExportView(View):
    """ Exports data from the application """
    def dispatch(self, request, *args, **kwargs):
        unauthorized = HttpResponse('Unauthorized', status=401)

        if request.user.is_anonymous() or not request.user.is_superuser:
            return unauthorized

        dump = {
            'server': {
                'tz': os.environ.get('TZ')
            },
            'modules': [ module.to_dict() for module in Module.objects.all() ],
            'users': [ user.to_dict() for user in User.objects.all() ]
        }

        return HttpResponse(json.dumps(dump, cls=encoders.JSONEncoder), status=200, content_type='application/json')