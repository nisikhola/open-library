from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from datetime import datetime, timedelta
from core import exceptions
import tarfile
import os
import shutil
import json
import binascii


def random_hex(length):
    return binascii.b2a(os.urandom(length))


def user_to_dict(self):
    profile_data = {}

    try:
        profile = self.profile
        fields = type(profile)._meta.get_fields()

        for field in fields:
            name = field.name

            if name not in ['id', 'user']:
                profile_data[name] = getattr(profile, name)

    except:
        pass

    return {
        'first_name': self.first_name,
        'last_name': self.last_name,
        'username': self.username,
        'email': self.email,
        'profile': profile_data,
        'is_staff': self.is_staff,
        'is_superuser': self.is_superuser,
        'sessions': [ session.to_dict() for session in self.sessions.all().order_by('start') ]
    }

User.add_to_class('to_dict', user_to_dict)


GENDER_CHOICES = [
    (0, 'UNDISCLOSED',),
    (1, 'MALE',),
    (2, 'FEMALE',),
]

class Student(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    gender = models.IntegerField(default=0, choices=GENDER_CHOICES)
    birthdate = models.DateField()
    cattle = models.IntegerField(null=True, blank=True)
    siblings = models.IntegerField(null=True, blank=True)
    has_toilet = models.NullBooleanField(null=True, blank=True)
    distance_to_school = models.IntegerField(null=True, blank=True, help_text='Number of minutes to walk to school')
    has_radio = models.NullBooleanField(null=True, blank=True)
    has_tv = models.NullBooleanField(null=True, blank=True)
    has_computer = models.NullBooleanField(null=True, blank=True)
    dat_001 = models.CharField(max_length=50, null=True, blank=True)
    dat_002 = models.CharField(max_length=50, null=True, blank=True)
    dat_003 = models.CharField(max_length=50, null=True, blank=True)
    dat_004 = models.CharField(max_length=50, null=True, blank=True)
    dat_005 = models.CharField(max_length=50, null=True, blank=True)
    dat_006 = models.CharField(max_length=50, null=True, blank=True)
    dat_007 = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.user.username


class Session(models.Model):
    user = models.ForeignKey(User, related_name='sessions')
    start = models.DateTimeField(auto_now_add=True)
    last_checkin = models.DateTimeField(auto_now=True)

    def to_dict(self):
        return {
            'start': self.start,
            'last_checkin': self.last_checkin,
            'accesses': [ access.to_dict() for access in self.accesses.all().order_by('start') ]
        }


class Access(models.Model):
    session = models.ForeignKey(Session, related_name='accesses')
    path = models.CharField(max_length=200)
    start = models.DateTimeField(auto_now_add=True)
    last_checkin = models.DateTimeField(auto_now=True)
    module = models.ForeignKey('Module', related_name='sessions')

    def to_dict(self):
        return {
            'path': self.path,
            'start': self.start,
            'last_checkin': self.last_checkin,
            'module': self.module.id
        }


class ModuleManager(models.Manager):
    def from_archive(self, archive):
        path = 'tmp/{}'.format(random_hex(16))

        def cleanup():
            shutil.rmtree(path)

        archive_path = archive.temporary_file_path()
        os.makedirs(path, exist_ok=True)

        try:
            with tarfile.open(archive_path) as tar:
                tar.extractall(path)

            with open('{}/module.json'.format(path)) as f:
                module_description = json.loads(f.read())

            try:
                with open('{}/injection.html'.format(path)) as f:
                    module_description['injection'] = f.read()

            except:
                module_description['injection'] = ''

            module = self.model(**module_description)

            content_path = '{}/content'.format(path)
            destination_path = 'modules/{}'.format(module_description.get('slug'))

            if not os.path.exists(content_path):
                raise Exception()

            if os.path.exists(destination_path):
                raise Exception()

            shutil.move(content_path, destination_path)
            module.save()

        except Exception:
            cleanup()
            raise exceptions.BadArchiveException()

        cleanup()
        return module


class Module(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    entry_point = models.CharField(max_length=100)
    injection = models.TextField(max_length=10000, null=True, blank=True)
    active = models.BooleanField(default=True)
    objects = ModuleManager()

    def __str__(self):
        return self.name

    def to_dict(self):
        return {
            'name': self.name,
            'slug': self.slug,
            'id': self.id,
            'active': self.active
        }

    def delete(self):
        try:
            shutil.rmtree('modules/{}'.format(self.slug))
        except:
            pass
        self.active = False
        self.save()