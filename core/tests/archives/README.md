# IMPORTANT

The folders in this directory are compiled into `.tar.gz` files during the test procedure.  After testing, the `.tar.gz` files are removed.

The purpose of these files is for testing the creation of `Module` objects from module archives.

