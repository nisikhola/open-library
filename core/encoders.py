import json
from datetime import date, datetime


class JSONEncoder(json.JSONEncoder):
    """ subclass of the JSONEncoder to handle special types """
    def default(self, o):
        if isinstance(o, datetime) or isinstance(o, date):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)
