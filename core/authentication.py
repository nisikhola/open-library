from rest_framework.authentication import SessionAuthentication 

class CsrfExemptSessionAuthentication(SessionAuthentication):
    """ Disables CSRF check for ViewSets """
    def enforce_csrf(self, request):
        return