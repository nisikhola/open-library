from django.test import TestCase
from django.contrib.auth.models import User
from django.core.files.uploadedfile import TemporaryUploadedFile
from unittest.mock import patch
from django import forms
from datetime import datetime, timedelta
from time import sleep
from core import encoders, exceptions
from .forms import *
from .models import *
import json
import shutil
import os


def login_required(self, url):
    """
    This is a test helper to check that a specific view implements the LoginRequiredMixin
    """
    res = self.client.get('/modules/test/')
    self.assertEqual(type(res).__name__, 'HttpResponseRedirect')
    self.assertEqual(res.url, '/login/?next=/modules/test/')


def check_recent(self, date_time_value):
    """
    Asserts that the datetime value is less than a second old
    """
    now = datetime.now()
    self.assertLess((now - date_time_value).seconds, 1)


def dump_and_parse(dictionary):
    """ Dumps then parses a dictionary; useful as this is what the test client does with HTTP requests """
    return json.loads(json.dumps(dictionary, cls=encoders.JSONEncoder))


class LoginViewTest(TestCase):
    def tearDown(self):
        Session.objects.all().delete()
        self.client.logout()

    def test_get_login_page(self):
        res = self.client.get('/login/')
        self.assertTemplateUsed(res, 'login.html')

    def test_successful_login(self):
        user = User.objects.create_user(username='test', password='test')
        res = self.client.post('/login/', data={'username': 'test', 'password': 'test'})
        session_id = self.client.session.get('session_id')
        session = Session.objects.get(id=session_id)
        now = datetime.now()
        self.assertLess((now - session.start).seconds, 1)
        self.assertEqual(session.user, user)

    def test_redirects_if_already_logged_in(self):
        user = User.objects.create_user(username='hi', password='there')
        self.client.force_login(user)
        res = self.client.get('/login/')
        self.assertRedirects(res, '/modules/')


class ModuleViewTest(TestCase):
    def test_login_required(self):
        login_required(self, '/modules/test/')


class ModuleListViewTest(TestCase):
    def test_login_required(self):
        login_required(self, '/modules/')


class AccessViewSetTest(TestCase):
    def setUp(self):
        self.module = Module.objects.create(name='test', slug='test', entry_point='/', injection='')

    def test_create_fails_if_not_logged_in(self):
        res = self.client.post('/api/accesses/')
        self.assertEqual(res.status_code, 401)

    def test_create_access_new_session(self):
        user = User.objects.create_user(username='hi', password='there')
        self.client.force_login(user)
        res = self.client.post('/api/accesses/', data={'path': '/test', 'module': self.module.id})
        data = res.json()
        session = Session.objects.get(id=data.get('session'))
        access = Access.objects.get(id=data.get('id'))
        self.assertEqual(self.client.session.get('session_id'), session.id)
        self.assertEqual(access.path, '/test')
        check_recent(self, access.last_checkin)
        check_recent(self, access.start)
        check_recent(self, session.start)
        check_recent(self, session.last_checkin)

    def test_create_uses_existing_session(self):
        user = User.objects.create_user(username='hi', password='there')
        self.client.force_login(user)
        session = Session.objects.create(user=user)
        tmp_session = self.client.session
        tmp_session['session_id'] = session.id
        tmp_session.save()

        res = self.client.post('/api/accesses/', data={'path': '/test', 'module': self.module.id})
        data = res.json()
        updated_session = Session.objects.get(id=data.get('session'))
        access = Access.objects.get(id=data.get('id'))
        self.assertEqual(self.client.session.get('session_id'), session.id)
        self.assertEqual(access.path, '/test')
        self.assertNotEqual(updated_session.last_checkin, session.last_checkin)
        check_recent(self, access.last_checkin)
        check_recent(self, access.start)
        check_recent(self, session.last_checkin)

    def test_update_fails_if_not_logged_in(self):
        res = self.client.put('/api/accesses/1/')
        self.assertEqual(res.status_code, 401)

    def test_update(self):
        user = User.objects.create_user(username='hi', password='there')
        self.client.force_login(user)

        session = Session.objects.create(user=user)
        access = Access.objects.create(path='/test', session=session, module=self.module)
        sleep(1.1) # let the session & access `last_checkin` fields age

        res = self.client.put('/api/accesses/{}/'.format(access.id))
        data = res.json()
        self.assertEqual(access.id, data.get('id'))

        access2 = Access.objects.get(id=data.get('id'))
        session2 = Session.objects.get(id=session.id)
        check_recent(self, access2.last_checkin)
        check_recent(self, session2.last_checkin)


class ExportViewAuthenticationTest(TestCase):
    def test_unauthenticated_is_unauthorized(self):
        res = self.client.get('/export/')
        self.assertEqual(res.status_code, 401)

    def test_non_superuser_is_unauthorized(self):
        user = User.objects.create_user(username='hi', password='there')
        self.client.force_login(user)
        res = self.client.get('/export/')
        self.assertEqual(res.status_code, 401)

    def test_superuser_is_authorized(self):
        user = User.objects.create_user(username='hi', password='there', is_superuser=True)
        self.client.force_login(user)
        res = self.client.get('/export/')
        self.assertEqual(res.status_code, 200)


class ExportViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', password='test', is_superuser=True)
        self.client.force_login(self.user)

    def test_returns_server_info(self):
        res = self.client.get('/export/')
        data = res.json()
        self.assertTrue('server' in data)
        self.assertTrue('tz' in data['server'])

    def test_returns_module_info(self):
        module = Module.objects.create(name='test', slug='test-slug', entry_point='index.html', injection='')
        res = self.client.get('/export/')
        data = res.json()
        self.assertTrue('modules' in data)
        self.assertEqual(data['modules'], [module.to_dict()])

    def test_returns_user_info(self):
        module = Module.objects.create(name='test', slug='test-slug', entry_point='index.html', injection='')
        session_start = datetime.now()
        session_end = session_start - timedelta(minutes=20)
        user = User.objects.create_user(username='dave', password='wat')
        student = Student.objects.create(gender=1, birthdate='1970-01-01', user=user)
        session = Session.objects.create(user=user, start=session_start, last_checkin=session_end)
        access = Access.objects.create(session=session, module=module, path='/test', last_checkin=session_end, start=session_start)

        res = self.client.get('/export/')
        data = res.json()
        self.assertTrue('users' in data)
        self.assertEqual(data['users'][0], dump_and_parse(self.user.to_dict()))
        self.assertEqual(data['users'][1], dump_and_parse(user.to_dict()))

    def test_sets_content_type(self):
        res = self.client.get('/export/')
        self.assertEqual(res._headers.get('content-type')[1], 'application/json')
