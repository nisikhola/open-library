# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-16 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20170616_1709'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='session',
            name='expires',
        ),
        migrations.AddField(
            model_name='session',
            name='last_checkin',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
