# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-16 17:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_session_expires'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='expires',
            field=models.DateTimeField(),
        ),
    ]
