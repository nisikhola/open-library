# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-16 23:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20170616_1758'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='entry_point',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
